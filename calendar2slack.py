from __future__ import print_function
import datetime
import pickle
import os.path
import timestamp

from datetime import timedelta
from datetimerange import DateTimeRange
from slackclient import SlackClient
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/calendar.readonly']

def main():
    """Shows basic usage of the Google Calendar API.
    Prints the start and name of the next 10 events on the user's calendar.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('calendar', 'v3', credentials=creds)

    # Call the Calendar API
    now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time

    # Getting the upcoming event
    print('Getting the upcoming event')
    events_result = service.events().list(calendarId='primary', timeMin=now,
                                        maxResults=2, singleEvents=True,
                                        orderBy='startTime').execute()
    events = events_result.get('items', [])

    if not events:
        print('No upcoming events found.')

    # Get event start time to set condition whether to set slack status
    # Also skip other events if full day event exist
    if events[0]['start'].get('date'):
        startDateTime = events[0]['start'].get('date')
        endTimeStr = events[0]['end'].get('date')
        endTime = datetime.datetime.strptime(endTimeStr, '%Y-%m-%d')
    else:
        startDateTime = events[0]['start'].get('dateTime').split('+')[0]

    # Get endTime as Unix time format for status expiration
        endTimeStr = events[0]['end'].get('dateTime').split('+')[0]
        endTime = datetime.datetime.strptime(endTimeStr, '%Y-%m-%dT%H:%M:%S') 

    endTimeUnix = endTime.timestamp()

    # Get summary for status_text
    summary = events[0]['summary']
    print(summary.lower())

    # Set slack status based on current time and event start time
    currentDateTime = datetime.datetime.now()
    time_range = DateTimeRange(currentDateTime, currentDateTime + timedelta(minutes=5))

    # Set find keyword for emoji
    if summary.lower().find('meeting'):
        emoji = ":calendar:"
    elif summary.lower().find('sick'):
        emoji = ":face_with_thermometer:"
    elif summary.lower().find('traveling'):
        emoji = ":airplane:"
    elif summary.lower().find('workout') or summary.lower.find('exercise'):
        emoji = ":tennis:"
    elif summary.lower().find('study'):
        emoji = ":book:"
    else:
        emoji = ":house:"

    if events[0]['start'].get('date') or startDateTime in time_range:
        slackToken = open("SLACK_API_TOKEN", "r").read().rstrip("\n\r")
        sc = SlackClient(slackToken)
        slackUserId = open("SLACK_USER_ID", "r").read().rstrip("\n\r")
        
        sc.api_call(
            "users.profile.set",
            user= slackUserId,
            profile= {
                "status_text": summary,
                "status_emoji": emoji,
                "status_expiration": endTimeUnix,
            },
        )
    else:
        pass

if __name__ == '__main__':
    main()
