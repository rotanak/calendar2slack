This is a `python3` script that will pull your event for Google Calendar and set slack status for user. First things first, you will need to retrieve some secrets.


# Pre-requisite

1. Slack Legacy token
    - Please go to [Slack API Legacy Token page](https://api.slack.com/custom-integrations/legacy-tokens) and generate a Legacy Token for your account workspace
    - Copy and save the Legacy token to `SLACK_API_TOKEN` file in this directory


2. Slack User ID
    - Go to Slack on your web browser
    - Click on your `username` on the top left of the screen
    - Click on `Profile & account`
    - Your profile will show on the right, click on the `vertical 3 dots` NEXT to `Edit Profile` 
    - Click on `Copy member ID`
    - Create a file in your app directory name `SLACK_USER_ID` and paste the `member ID` in the file and save it



3. Google Calendar API 
    - Go to [Google Calendar API python](https://developers.google.com/calendar/quickstart/python)
    - Click on `Enable the Google Calendar API` and download the `credentials.json` to this directory


4. Install the `requirements.txt`: `pip3 install -r requirement.txt`


5. Schedule to run `python3 calendar2slack.py` every 2 to 3  minutes


# How it works

`calendar2slack.py` will pull events from your primary calendar.

1. If there's a full day event, it will not set slack status for other events on the same day.


2. If the event start time is within 5 minutes of the script scheduled run time, it will set slack status along with expiration (status will disappear at your specified event end time).


3. Emoji, the script will check keywords in your event title and set emoji:

`meeting` => :calendar:

`sick` => :face_with_thermometer:

`traveling` => :airplane:

`workout` or `exercise` => :tennis:

`study` => :book:

others => :house: 
